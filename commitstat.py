#!/usr/bin/python3

"""Generates commit data statistics for various VCS.

This script measures team performance of Git repositories through
the metrics of:

    - frequency of committer,
    - number of lines added,
    - number of lines deleted.

The required data is fetched by SSHing into Alioth.
"""

import logging
import os
import sys
import psycopg2
import urllib.request, urllib.parse, urllib.error
import json    

# cachedir='/var/cache/teammetrics/commits'
cachedir='/srv/blends.debian.org/www'
downloadurl='http://teammetrics.alioth.debian.org/commitstats/'
commits='commits.json'

LOG_FILE = 'commitstat.log'
LOG_SAVE_DIR = '/var/log/teammetrics'
LOG_FILE_PATH = os.path.join(LOG_SAVE_DIR, LOG_FILE)

DATABASE = {
            'name':        'teammetrics',
            'defaultport': 5432,
            'port':        5452, # ... use this on blends.debian.net / udd.debian.net
           }

def start_logging():
    """Initialize the logger."""
    logging.basicConfig(filename=LOG_FILE_PATH,
                        level=logging.INFO,
                        format='%(asctime)s %(levelname)s: %(message)s')

if __name__ == '__main__':

    start_logging()
    logging.info('\t\tStarting CommitStat')

    # get_stats()
    os.system('mkdir -p '+cachedir)

#    urllib.urlretrieve (downloadurl+'/'+commits+'.xz', cachedir+'/'+commits+'.xz')
    os.system('unxz --keep -f '+cachedir+'/'+commits+'.xz')

    comfp = open(cachedir+'/'+commits)
    # comfp = open(cachedir+'/zw.json')
    data = json.load(comfp)

    try:
        conn = psycopg2.connect(database=DATABASE['name'])
        cur = conn.cursor()
    except psycopg2.OperationalError:
        try: 
            conn = psycopg2.connect(database=DATABASE['name'], port=DATABASE['port'])
            cur = conn.cursor()
        except psycopg2.Error as detail:
            logging.error(detail)
            sys.exit(1)

    query = "DELETE FROM commitstat"
    cur.execute(query)

    query = """PREPARE git_insert
      AS INSERT INTO commitstat (commit_id, project, package, name, commit_date)
      VALUES ($1, $2, $3, $4, $5)"""
    cur.execute(query)

    gitquery = "EXECUTE git_insert (%(commit_id)s, %(project)s, %(package)s, %(name)s, %(commit_date)s)"
    # FIXME: to avoid trying to insert duplicate inserts (no idea why there are duplicates)
    #        the just stored commits are recorded and will be checked before trying to insert again
    seen_commits = []
    for prj in data:
        if 'git' in prj:
            for commits in prj['git']:
                if commits['commits']:
                    for com in commits['commits']:
                        if com['commit_id'] in seen_commits:
                            print("Dupplicated Git commit in project:%s, package:%s, id:%s" % (prj['project'], commits['package'], com['commit_id']))
                            continue
                        com['project'] = prj['project']
                        com['package'] = commits['package']
                        try:
                            cur.execute(gitquery, com)
                        except psycopg2.IntegrityError as err:
                            print("project:%s, package:%s" % (com['project'], com['package']))
                            print(err)
                        seen_commits.append(com['commit_id'])
                else:
                    print("No commits for package %s of project %s" % (commits['package'], prj['project']))

    conn.commit()
    cur.close()
    conn.close()
