/*
 * Try to reproduce results from
 *
 *      https://people.debian.org/~eriberto/udd/uploaders_ranking.html
 *
 *  Source at https://anonscm.debian.org/git/users/eriberto/debian-udd-scripts.git/tree/
 *
 *
 *  If you call
 *     psql -q udd < THIS_SCRIPT
 *  the output is less verbose, you can even try
 *     psql -q -t udd < THIS_SCRIPT
 *  to get pure data only
 */

-- provide a table with carnivore_id, year and number of uploads in this year
CREATE OR REPLACE FUNCTION carnivore_id_uploads_year() RETURNS SETOF RECORD AS $$
  SELECT ce.id, EXTRACT(year FROM uh.date)::int AS year, count(*)
    FROM upload_history uh
    JOIN all_sources ON uh.source=all_sources.source AND uh.version=all_sources.version
    JOIN carnivore_emails ce ON ce.email = uh.changed_by_email
     WHERE all_sources.distribution = 'debian' AND all_sources.release = 'sid'   -- restriction to different packages, no uploads of different version of same package
       AND nmu = 'f'
   GROUP BY id, year
$$ LANGUAGE SQL ;

-- Test function above
-- SELECT * FROM carnivore_id_uploads_year() AS (id int, year int, count bigint) ORDER BY count DESC;

-- deal with the fact that carnivore names are not uniqe
-- the function makes sure only one name is choosen (but not a stupid one starting with 'Replacement key for'
CREATE OR REPLACE FUNCTION unique_carivore_names() RETURNS SETOF RECORD AS $$
  SELECT id, name FROM (
    SELECT *, ROW_NUMBER() OVER (PARTITION BY id ORDER BY name) FROM carnivore_names -- carnivore_names is not unique unfortunately :-(
      WHERE name not like 'Replacement key for%'
  ) tmp WHERE ROW_NUMBER = 1
$$ LANGUAGE SQL ;

-- Table with all time uploads sorted by number of uploads
SELECT cn.name, sum FROM (
     SELECT id, SUM(count) AS sum FROM carnivore_id_uploads_year() AS (id int, year int, count bigint) GROUP BY id
   ) s
   JOIN (SELECT * FROM unique_carivore_names() AS (id int, name text) ) cn ON s.id = cn.id
   ORDER BY sum DESC;

-- Table with last years uploads sorted by number of uploads
SELECT cn.name, sum FROM (
     SELECT id, SUM(count) AS sum FROM carnivore_id_uploads_year() AS (id int, year int, count bigint) WHERE year = EXTRACT(year FROM current_date)::int-1 GROUP BY id
   ) s
   JOIN (SELECT * FROM unique_carivore_names() AS (id int, name text) ) cn ON s.id = cn.id
   ORDER BY sum DESC;
